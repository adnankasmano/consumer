package com.javasampleapproach.rabbitmq.Producer;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Producer {
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Value("${jsa1.rabbitmq.exchange}")
    private String exchange;

    @Value("${jsa1.rabbitmq.routingkey}")
    private String routingKey;

    public void produceMsg(String msg) {
        System.out.println("Sending " + msg + " through rabbit mq");
        amqpTemplate.convertAndSend(exchange, routingKey, msg);
        System.out.println("done");

    }
}
