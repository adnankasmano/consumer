package com.javasampleapproach.rabbitmq.consumer;

import com.javasampleapproach.rabbitmq.Producer.Producer;
import com.javasampleapproach.rabbitmq.model.message;
import com.javasampleapproach.rabbitmq.repositories.MessageRepository;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    @Autowired
    private MessageRepository msrp ;

    @Autowired
    private Producer producer;

	@RabbitListener(queues="${jsa.rabbitmq.queue}")
	public void recievedMessage(String msg) {
        System.out.println("Recieved Message: " + msg);
        message m = new message();
        m.setMsg(msg);
        msrp.save(m);

        producer.produceMsg("Respond From DB");





    }
}
