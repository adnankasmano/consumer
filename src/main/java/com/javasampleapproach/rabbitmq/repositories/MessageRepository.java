package com.javasampleapproach.rabbitmq.repositories;

import com.javasampleapproach.rabbitmq.consumer.Consumer;
import com.javasampleapproach.rabbitmq.model.message;
import org.springframework.data.repository.CrudRepository;

public interface MessageRepository extends CrudRepository<message,Long> {

}
